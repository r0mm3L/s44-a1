// get post data

// fetch('https://jsonplaceholder.typicode.com/posts').then((response) => response
// 	.json()).then((data) => showPosts(data))

fetch('https://jsonplaceholder.typicode.com/posts').then((response) => {
	return response.json()
})
.then((data) => {
	return showPosts(data)
})

// add post

document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts', {



		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('Post successfully added')

		document.querySelector('#txt-title').value = null; 
		document.querySelector('#txt-body').value = null; 
	})
})

// show post

const showPosts = (posts) => {
	let postEntries = ''

	posts.forEach((post) => {
		postEntries += `
			<div id = "post-${post.id}">
				<h2 id = "post-title-${post.id}">${post.title}</h2>
				<p id = "post-body-${post.id}">${post.body}</p>

				<button onClick = "editPost('${post.id}')">Edit</button>
				<button onClick = "deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}


// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
}


// update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {


	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert('edited')

		document.querySelector('#txt-edit-id').value = null
		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null
		document.querySelector('#btn-submit-update').removeAttribute('disabled', true)
	})

})

// delete
const deletePost = (id) => {


fetch("https://jsonplaceholder.typicode.com/posts", {
   method: "DELETE"
}).then(response => {
   console.log(response.status);

	// posts = posts.filter((post) => {
	// 	if(post.id.toString() !== id){
	// 		return post
	// // 	}
	// });

	document.querySelector(`#post-${id}`).remove();

});
}



// const deletePost = (id) => {


// fetch("https://jsonplaceholder.typicode.com/posts/1", {
//    method: "DELETE"
// }).then(response => {
//    console.log(response.status);


// });
// let title = document.querySelector(`#post-title-${id}`).innerHTML = "";
// let body = document.querySelector(`#post-body-${id}`).innerHTML = "";

// }





